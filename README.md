# FightStore

[![pipeline status](https://gitlab.com/howlongtilthefight/fightstore/badges/master/pipeline.svg)](https://gitlab.com/howlongtilthefight/fightstore/commits/master)
[![coverage report](https://gitlab.com/howlongtilthefight/fightstore/badges/master/coverage.svg)](https://howlongtilthefight.gitlab.io/fightstore/tests/coverage)

Scrape, persist, and expose event data.
