import com.github.benmanes.gradle.versions.updates.DependencyUpdatesTask
import org.gradle.api.tasks.testing.logging.TestLogEvent

plugins {
    id("com.github.ben-manes.versions") version "0.30.0"
    id("org.springframework.boot") version "2.3.3.RELEASE"
    id("io.spring.dependency-management") version "1.0.10.RELEASE"
    java
    jacoco
}

springBoot {
    mainClassName = "com.howlongtilthefight.fightstore.FightstoreApplication"
    buildInfo()
}

java {
    sourceCompatibility = JavaVersion.VERSION_11
    targetCompatibility = JavaVersion.VERSION_11
}

group = "com.howlongtilthefight"
version = "0.0.1-SNAPSHOT"

val jsoupVersion = "1.13.1"
val nattyVersion = "0.13"
val springCloudVersion = "Hoxton.SR8"
val wiremockVersion = "2.27.1"

repositories {
    mavenCentral()
}

dependencyManagement {
    imports {
        mavenBom("org.springframework.cloud:spring-cloud-dependencies:$springCloudVersion")
    }
    dependencies {
        dependency("org.jsoup:jsoup:$jsoupVersion")
        dependency("com.joestelmach:natty:$nattyVersion")
        dependency("com.github.tomakehurst:wiremock-jre8:$wiremockVersion")
    }
}

dependencies {
    // Spring Boot dependencies
    implementation("org.springframework.boot:spring-boot-starter-actuator")
    implementation("org.springframework.boot:spring-boot-starter-data-mongodb-reactive")
    implementation("org.springframework.boot:spring-boot-starter-webflux")

    // Configuration/Lombok annotation processor dependencies
    annotationProcessor("org.projectlombok:lombok")
    annotationProcessor("org.springframework.boot:spring-boot-configuration-processor")
    compileOnly("org.projectlombok:lombok")

    // HTML dependencies
    implementation("org.jsoup:jsoup")
    implementation("com.joestelmach:natty")

    // Metrics dependencies
    implementation("io.micrometer:micrometer-registry-prometheus")

    // JUnit Jupiter dependencies
    testImplementation("org.junit.jupiter:junit-jupiter-api")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine")

    // Spring test dependencies
    testImplementation("de.flapdoodle.embed:de.flapdoodle.embed.mongo")
    testImplementation("io.projectreactor:reactor-test")
    testImplementation("org.springframework.boot:spring-boot-starter-test")

    // Http Integration dependencies
    testImplementation("com.github.tomakehurst:wiremock-jre8")
}

tasks.withType<Test> {
    useJUnitPlatform()
}

fun isStable(version: String): Boolean {
    val stableKeyword = listOf("RELEASE", "FINAL", "GA").any { version.toUpperCase().contains(it) }
    val stableRegex = "^[0-9,.v-]+(-r)?$".toRegex()
    val srRegex = "^[A-z]+\\.SR\\d+$".toRegex()
    val milestoneRegex = "^[\\w.]+-M\\d+\$".toRegex()
    val matchesStable = stableKeyword || stableRegex.matches(version) || srRegex.matches(version)
    val matchesUnstable = milestoneRegex.matches(version)
    return matchesStable && matchesUnstable.not()
}

tasks.named("dependencyUpdates", DependencyUpdatesTask::class.java).configure {
    rejectVersionIf {
        isStable(currentVersion) && !isStable(candidate.version)
    }
}
