plugins {
    id("com.gradle.enterprise").version("3.4.1")
}
rootProject.name = "fightstore"

val inCiEnv = !System.getenv("CI").isNullOrEmpty()

gradleEnterprise {
    buildScan {
        termsOfServiceUrl = "https://gradle.com/terms-of-service"
        termsOfServiceAgree = "yes"

        if (inCiEnv) {
            isUploadInBackground = false
            publishAlways()
            tag("CI")
        }
    }
}

buildCache {
    local {
        isEnabled = inCiEnv
        directory = file("cache/gradle")
        isPush = true
        removeUnusedEntriesAfterDays = 30
    }
}
