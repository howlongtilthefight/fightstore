package com.howlongtilthefight.fightstore;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FightstoreApplication {

    public static void main(final String[] args) {
        SpringApplication.run(FightstoreApplication.class, args);
    }
}
