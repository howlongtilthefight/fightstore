package com.howlongtilthefight.fightstore.client;

import com.howlongtilthefight.fightstore.domain.Fight;
import reactor.core.publisher.Flux;

public interface FightClient {
    Flux<Fight> getEvents(Integer referenceYear);
}
