package com.howlongtilthefight.fightstore.client;

import com.howlongtilthefight.fightstore.config.properties.ExternalProviderProperties;
import com.howlongtilthefight.fightstore.domain.Fight;
import com.joestelmach.natty.DateGroup;
import com.joestelmach.natty.Parser;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;

@Service
public class TapologyClient implements FightClient {

    private final WebClient webClient;

    @Autowired
    public TapologyClient(ExternalProviderProperties externalProviderProperties,
                          WebClient.Builder webClientBuilder) {
        webClient = webClientBuilder
                .baseUrl(externalProviderProperties.getTapology().getUrl())
                .defaultHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8")
                .defaultHeader("Accept-Encoding", "gzip, deflate, br")
                .defaultHeader("Accept-Language", "en-US,en;q=0.9")
                .defaultHeader("Cache-Control", "no-cache")
                .defaultHeader("Connection", "keep-alive")
                .defaultHeader("DNT", "1")
                .defaultHeader("Host", "www.tapology.com")
                .defaultHeader("Pragma", "no-cache")
                .defaultHeader("Upgrade-Insecure-Requests", "1")
                .defaultHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36")
                .build();
    }

    public Flux<Fight> getEvents(final Integer referenceYear) {
        return getUpcomingEventsPage()
                .flatMapIterable(TapologyClient::toElements)
                .map(element -> toFight(element, referenceYear));
    }

    private Mono<String> getUpcomingEventsPage() {
        return webClient.get()
                .uri(uriBuilder -> uriBuilder
                        .path("/fightcenter")
                        .queryParam("group", "major")
                        .queryParam("schedule", "upcoming")
                        .build())
                .retrieve()
                .bodyToMono(String.class)
                .name("get-page")
                .log("client.", Level.FINE)
                .metrics()
                .onErrorResume(Mono::error);
    }

    private static Elements toElements(final String responseBody) {
        return Jsoup.parse(responseBody)
                .getElementsByClass("fightcenterEvents")
                .first()
                .getElementsByTag("section");
    }

    private static Fight toFight(final Element e, final int referenceYear) {
        return Fight.builder()
                .eventUrlPath(parseNullableAttr(e.selectFirst(".promotion .name a"), "href"))
                .eventDate(parseEventDate(parseNullableText(e.selectFirst(".datetime")), referenceYear))
                .eventOrg(parseNullableAttr(e.selectFirst(".promotionLogo img"), "alt"))
                .eventName(parseNullableText(e.selectFirst(".promotion .name a")))
                .eventBout(parseNullableText(e.selectFirst(".bout a")))
                .eventBilling(parseNullableText(e.selectFirst(".billing")))
                .geoCountry(parseNullableAttr(e.selectFirst(".eventFlag"), "alt"))
                .geoRegion(parseNullableText(e.selectFirst(".region a")))
                .geoVenue(parseNullableText(e.selectFirst(".venue")))
                .geoVenueLocation(parseNullableText(e.selectFirst(".venue-location")))
                .build();
    }

    private static String parseNullableText(final Element element) {
        return Optional.ofNullable(element)
                .map(Element::ownText)
                .orElse(null);
    }

    private static String parseNullableAttr(final Element element, final String attributeKey) {
        return Optional.ofNullable(element)
                .map(e -> e.attr(attributeKey))
                .orElse(null);
    }

    @SuppressWarnings("ReturnOfNull")
    private static Date parseEventDate(final String dateString, final int referenceYear) {
        if (dateString == null) return null;

        final List<DateGroup> dateGroups = new Parser().parse(dateString);
        final DateGroup dateGroup = dateGroups.get(0);

        final Date assumedDate = dateGroup.getDates().get(0);
        final Calendar calendar = new GregorianCalendar();
        calendar.setTime(assumedDate);

        if (calendar.get(Calendar.YEAR) < referenceYear) {
            calendar.set(Calendar.YEAR, referenceYear + 1);
        }

        return calendar.getTime();
    }
}
