package com.howlongtilthefight.fightstore.config;

import com.howlongtilthefight.fightstore.config.properties.ExternalProviderProperties;
import com.howlongtilthefight.fightstore.config.properties.FightStoreProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties({
        ExternalProviderProperties.class,
        FightStoreProperties.class,
})
public class PropertyConfig {
}
