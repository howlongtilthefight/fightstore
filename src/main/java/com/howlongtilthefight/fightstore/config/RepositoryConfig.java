package com.howlongtilthefight.fightstore.config;

import com.howlongtilthefight.fightstore.repositories.FightRepository;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.repository.config.EnableReactiveMongoRepositories;

@Configuration
@EnableReactiveMongoRepositories(basePackageClasses = FightRepository.class)
public class RepositoryConfig {
}
