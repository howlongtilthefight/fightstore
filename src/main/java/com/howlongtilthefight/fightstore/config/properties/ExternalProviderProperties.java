package com.howlongtilthefight.fightstore.config.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Property definitions for unaware third party producers.
 * Such producers are not beholden to any contracts or
 * consumers. Consumers should expect breaking changes
 * to be introduced at any time, and are individually
 * responsible for respecting upstream license agreements.
 */
@ConfigurationProperties("external")
public class ExternalProviderProperties {

    private final TapologyProperties tapology = new TapologyProperties();

    public TapologyProperties getTapology() {
        return tapology;
    }

    public static class TapologyProperties {
        /**
         * Fully qualified base URL
         */
        private String url;

        public String getUrl() {
            return url;
        }

        public void setUrl(final String url) {
            this.url = url;
        }
    }
}
