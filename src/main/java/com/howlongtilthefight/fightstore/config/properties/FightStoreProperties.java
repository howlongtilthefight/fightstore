package com.howlongtilthefight.fightstore.config.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("fightstore")
public class FightStoreProperties {
}
