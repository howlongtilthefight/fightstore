package com.howlongtilthefight.fightstore.controller;

import com.howlongtilthefight.fightstore.domain.Fight;
import com.howlongtilthefight.fightstore.service.FightStoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping(path = "/api/fights")
public class FightController {

    private final FightStoreService fightStoreService;

    @Autowired
    public FightController(final FightStoreService fightStoreService) {
        this.fightStoreService = fightStoreService;
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public Flux<Fight> getLatestFights() {
        return fightStoreService.getLatestFights();
    }

    @GetMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<Fight> getFightById(final @PathVariable("id") String id) {
        return fightStoreService.getFightById(id);
    }
}
