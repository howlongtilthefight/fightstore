package com.howlongtilthefight.fightstore.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.With;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Data
@Builder
@Document
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class Fight {

    @Id
    @With
    @JsonSerialize(using = ToStringSerializer.class)
    private ObjectId _id;

    @Transient
    @Getter(AccessLevel.NONE)
    @Setter(AccessLevel.NONE)
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private transient Date _timestamp;

    private Date eventDate;
    private String eventOrg;
    private String eventName;
    private String eventBout;
    private String eventBilling;

    private String geoCountry;
    private String geoRegion;
    private String geoVenue;
    private String geoVenueLocation;

    private String eventUrlPath;

    public int get_timestamp() {
        return _id == null ? 0 : _id.getTimestamp();
    }
}
