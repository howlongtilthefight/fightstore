package com.howlongtilthefight.fightstore.repositories;

import com.howlongtilthefight.fightstore.domain.Fight;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Date;

@NoRepositoryBean
public interface FightRepository extends ReactiveCrudRepository<Fight, String> {
    Flux<Fight> findAllByEventDateAfter(final Date date);

    Flux<Fight> findAllByEventDateBefore(final Date date);

    Flux<Fight> findAllByEventDateBetween(final Date from, final Date to);

    Mono<Fight> findTopByEventUrlPath(final String urlPath);
}
