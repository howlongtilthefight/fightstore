package com.howlongtilthefight.fightstore.repositories;

import com.howlongtilthefight.fightstore.domain.Fight;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Date;

public interface MongoFightRepository extends FightRepository, ReactiveMongoRepository<Fight, String> {
    @Override
    Flux<Fight> findAllByEventDateAfter(final Date date);

    @Override
    Flux<Fight> findAllByEventDateBefore(final Date date);

    @Override
    Flux<Fight> findAllByEventDateBetween(final Date from, final Date to);

    @Override
    Mono<Fight> findTopByEventUrlPath(final String urlPath);
}
