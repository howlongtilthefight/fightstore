package com.howlongtilthefight.fightstore.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Clock;
import java.time.Instant;
import java.time.Year;
import java.util.Date;

@Service
public class DateTimeService {

    private final Clock clock;

    @Autowired
    public DateTimeService(final Clock clock) {
        this.clock = clock;
    }

    public Date now() {
        return Date.from(Instant.now(clock));
    }

    public int getCurrentYear() {
        return Year.now(clock).getValue();
    }
}
