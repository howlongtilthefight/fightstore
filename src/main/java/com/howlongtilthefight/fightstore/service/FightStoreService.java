package com.howlongtilthefight.fightstore.service;

import com.howlongtilthefight.fightstore.domain.Fight;
import com.howlongtilthefight.fightstore.repositories.FightRepository;
import com.howlongtilthefight.fightstore.workhorse.PaperScraperService;
import org.reactivestreams.Publisher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class FightStoreService {

    private final FightRepository repository;
    private final PaperScraperService paperScraper;

    @Autowired
    public FightStoreService(final FightRepository repository,
                             final PaperScraperService paperScraper) {
        this.repository = repository;
        this.paperScraper = paperScraper;
    }

    public Mono<Fight> getFightById(final String id) {
        return repository.findById(id)
                .name("get-by-id")
                .log()
                .metrics();
    }

    public Flux<Fight> getLatestFights() {
        return Flux.from(paperScraper.requestFights())
                .name("request-fights")
                .log()
                .metrics()
                .window(Integer.MAX_VALUE)
                .flatMap(this::storeFights)
                .thenMany(repository.findAllByEventDateAfter(paperScraper.getCurrentDate()));
    }

    Flux<Fight> storeFights(final Publisher<Fight> fights) {
        return repository.saveAll(
                Flux.from(fights)
                        .filter(fight -> fight.getEventUrlPath() != null)
                        .doOnDiscard(Fight.class, System.err::println)
                        .flatMap(this::mergeIdIfExists))
                .name("save-all")
                .log()
                .metrics();
    }

    private Mono<Fight> mergeIdIfExists(final Fight fight) {
        return repository.findTopByEventUrlPath(fight.getEventUrlPath())
                .map(existingFight -> fight.with_id(existingFight.get_id()))
                .defaultIfEmpty(fight);
    }
}
