package com.howlongtilthefight.fightstore.workhorse;

import com.howlongtilthefight.fightstore.client.FightClient;
import com.howlongtilthefight.fightstore.domain.Fight;
import com.howlongtilthefight.fightstore.service.DateTimeService;
import org.reactivestreams.Publisher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.concurrent.atomic.AtomicReference;

@Service
public class PaperScraperService {

    private static final long RATE_LIMIT_SECONDS = 60L * 60L;

    private final DateTimeService dateTimeService;
    private final FightClient fightClient;

    private final AtomicReference<Instant> lastLocal;
    private final AtomicReference<Long> countDownerSeconds;

    @Autowired
    public PaperScraperService(final DateTimeService dateTimeService,
                               final FightClient fightClient) {
        this.dateTimeService = dateTimeService;
        this.fightClient = fightClient;

        lastLocal = new AtomicReference<>(dateTimeService.now().toInstant());
        countDownerSeconds = new AtomicReference<>(RATE_LIMIT_SECONDS);
    }

    public Publisher<Fight> requestFights() {
        final int year = LocalDate.ofInstant(lastLocal.get(), ZoneId.of("UTC")).getYear();

        if (timesUp()) return fightClient.getEvents(year);
        else return Mono.empty();
    }

    public Date getCurrentDate() {
        return dateTimeService.now();
    }

    private boolean timesUp() {
        final Instant newNow = dateTimeService.now().toInstant();
        final Instant prev = lastLocal.getAndSet(newNow);

        final long diff = newNow.getEpochSecond() - prev.getEpochSecond();
        final Long secondsRemaining = countDownerSeconds.updateAndGet(sec -> sec - diff);

        return secondsRemaining <= 0;
    }
}
