package com.howlongtilthefight.fightstore.client;

import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.matching.EqualToPattern;
import com.github.tomakehurst.wiremock.matching.RequestPatternBuilder;
import com.howlongtilthefight.fightstore.config.properties.ExternalProviderProperties;
import com.howlongtilthefight.fightstore.domain.Fight;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.web.reactive.function.client.WebClientAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientResponseException;
import reactor.test.StepVerifier;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Date;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.urlPathEqualTo;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig;
import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = {WebClientAutoConfiguration.class})
class TapologyClientTest {

    private final static int YEAR_2018 = 2018;

    private static WireMockServer server;

    @Autowired
    private WebClient.Builder webClientBuilder;

    private TapologyClient client;

    @BeforeAll
    static void segaGenesis() {
        server = new WireMockServer(wireMockConfig().dynamicPort());
        server.start();
    }

    @AfterAll
    static void shutItDown() {
        server.shutdown();
    }

    @BeforeEach
    void setUp() {
        var properties = new ExternalProviderProperties();
        properties.getTapology().setUrl(server.baseUrl());
        client = new TapologyClient(properties, webClientBuilder);
        StepVerifier.setDefaultTimeout(Duration.ofSeconds(10));
    }

    @Nested class MostlyCompleteData {

        @BeforeEach
        void stubResponses() {
            server.stubFor(get(urlPathEqualTo("/fightcenter"))
                    .withQueryParam("group", new EqualToPattern("major"))
                    .withQueryParam("schedule", new EqualToPattern("upcoming"))
                    .willReturn(aResponse()
                            .withHeader("Content-Type", "text/html; charset=utf-8")
                            .withHeader("Content-Security-Policy", "upgrade-insecure-requests")
                            .withStatus(HttpStatus.OK.value())
                            .withBodyFile("tapology/fightcenter.html")));
        }

        @AfterEach
        void tidyUp() {
            server.verify(RequestPatternBuilder.allRequests());
            server.resetRequests();
        }

        @Test
        void getEvents_returnsParsedData() {
            StepVerifier.create(client.getEvents(YEAR_2018))
                    .consumeNextWith(fight -> assertThat(fight).isEqualTo(
                            Fight.builder()
                                    .eventDate(toEstDate(2018, Month.NOVEMBER, 29, 19, 30))
                                    .eventOrg("LFA")
                                    .eventName("USACA")
                                    .eventBout("Calhoune vs. Carlos")
                                    .eventBilling("Main Card")
                                    .geoCountry("Us")
                                    .geoRegion("US Southwest Region")
                                    .geoVenue("The Bomb Factory")
                                    .geoVenueLocation("Dallas, Texas")
                                    .eventUrlPath("/fightcenter/events/56676-usaca-black-tie-brawl-5")
                                    .build()))
                    .thenCancel()
                    .verify();
        }

        @Test
        void getEvents_whenParsedDateIsPast_setsCurrentYearPlusOne() {
            // The natty library is smart enough to parse Saturday, February 02 to the last year
            // that day/date combo was valid, but in this case it chooses 2013 over 2019
            final Date expectedDate = toEstDate(2019, Month.FEBRUARY, 2, 21, 0);
            StepVerifier.create(client.getEvents(YEAR_2018))
                    .expectNextCount(20)
                    .consumeNextWith(fight -> assertThat(fight.getEventDate()).isEqualTo(expectedDate))
                    .expectNextCount(4)
                    .verifyComplete();
        }
    }

    @Nested class LimitedData {

        static final int TOTAL_TEST_DOUBLES = 12;

        @BeforeEach
        void stubResponses() {
            server.stubFor(get(urlPathEqualTo("/fightcenter"))
                    .withQueryParam("group", new EqualToPattern("major"))
                    .withQueryParam("schedule", new EqualToPattern("upcoming"))
                    .willReturn(aResponse()
                            .withHeader("Content-Type", "text/html")
                            .withStatus(HttpStatus.OK.value())
                            .withBodyFile("tapology/fightcenter_incompleteListingsData.html")));
        }

        @Test
        void getEvents_withMissingFields_doesNotThrowError() {
            StepVerifier.create(client.getEvents(YEAR_2018))
                    .expectNextCount(TOTAL_TEST_DOUBLES)
                    .verifyComplete();
        }

        @Test
        void getEvents_withMissingFields_parsesAllAvailable() {
            StepVerifier.create(client.getEvents(YEAR_2018))
                    .assertNext(fight ->
                            assertThat(fight.getEventDate()).isEqualTo(toEstDate(2018, Month.NOVEMBER, 29, 19, 30)))
                    .as("1. eventDate only")
                    .assertNext(fight -> assertThat(fight.getEventOrg()).isEqualTo("BFC"))
                    .as("2. eventOrg only")
                    .assertNext(fight -> {
                        assertThat(fight.getEventName()).isEqualTo("LFA 55");
                        assertThat(fight.getEventUrlPath())
                                .isEqualTo("/fightcenter/events/56314-lfa-55-johns-vs-yanez");
                    })
                    .as("3. eventName and eventUrlPath only")
                    .assertNext(fight -> assertThat(fight.getEventBout()).isEqualTo("dos Anjos vs. Usman"))
                    .as("4. eventBout only")
                    .assertNext(fight -> assertThat(fight.getEventBilling()).isEqualTo("Main Event"))
                    .as("5. eventBilling only")
                    .assertNext(fight -> assertThat(fight.getGeoCountry()).isEqualTo("It"))
                    .as("6. geoCountry only")
                    .assertNext(fight -> assertThat(fight.getGeoRegion()).isEqualTo("Australia & New Zealand Region"))
                    .as("7. geoRegion only")
                    .assertNext(fight -> assertThat(fight.getGeoVenue()).isEqualTo("Axiata Arena"))
                    .as("8. geoVenue only")
                    .assertNext(fight -> assertThat(fight.getGeoVenueLocation()).isEqualTo("Prior Lake, Minnesota"))
                    .as("9. geoVenueLocation only")
                    .assertNext(fight -> assertThat(fight).isEqualToComparingFieldByField(Fight.builder().build()))
                    .as("10. empty listing")
                    .assertNext(fight -> {
                        assertThat(fight.getEventUrlPath()).isEqualTo("/fightcenter/events/55630-bellator-211");
                        assertThat(fight.getEventName()).isEqualTo("Bellator 212");
                        assertThat(fight.getEventDate()).isEqualTo(toEstDate(2018, Month.DECEMBER, 14, 21, 0));
                    }).as("11. eventUrl, eventName, and eventDate only")
                    .assertNext(fight -> assertThat(fight.getEventUrlPath()).isEqualTo("/fightcenter/events/54138-ufc-fight-night-143"))
                    .as("12. eventUrl only")
                    .verifyComplete();
        }
    }

    @Nested class ErrorHandling {

        @Test
        void getEvents_onErrorCodeResponse_propagatesError() {
            server.stubFor(get(urlPathEqualTo("/fightcenter"))
                    .withQueryParam("group", new EqualToPattern("major"))
                    .withQueryParam("schedule", new EqualToPattern("upcoming"))
                    .willReturn(aResponse()
                            .withHeader("Content-Type", "text/html")
                            .withHeader("Transfer-Encoding", "chunked")
                            .withStatus(HttpStatus.NOT_FOUND.value())
                            .withBodyFile("tapology/404.html")));

            StepVerifier.create(client.getEvents(YEAR_2018))
                    .expectError(WebClientResponseException.class)
                    .verifyThenAssertThat()
                    .hasNotDroppedElements();
        }
    }

    private static Date toEstDate(final int year, final Month month, final int day, final int hour, final int min) {
        final LocalDateTime localDt = LocalDateTime.of(year, month, day, hour, min);
        final ZoneId zoneId = ZoneId.of("US/Eastern");
        final long nowMillis = ZonedDateTime.of(localDt, zoneId).toInstant().toEpochMilli();

        return new Date(nowMillis);
    }
}
