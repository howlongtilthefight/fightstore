package com.howlongtilthefight.fightstore.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.howlongtilthefight.fightstore.config.PropertyConfig;
import com.howlongtilthefight.fightstore.domain.Fight;
import com.howlongtilthefight.fightstore.service.FightStoreService;
import org.bson.types.ObjectId;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;
import org.springframework.test.web.reactive.server.EntityExchangeResult;
import org.springframework.test.web.reactive.server.FluxExchangeResult;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.io.IOException;
import java.time.Instant;
import java.util.Date;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.util.Maps.newHashMap;
import static org.mockito.Mockito.when;

@SpringJUnitConfig(classes = {FightController.class, PropertyConfig.class})
@WebFluxTest(controllers = FightController.class)
class FightControllerTest {

    @Autowired
    private WebTestClient client;

    @MockBean
    private FightStoreService fightStoreService;

    private final Instant now = Instant.parse("2016-04-20T06:09:10Z");
    private final Date nowDate = Date.from(now);

    @Test
    void getLatestFights() {
        final Fight fightOfTheNight = Fight.builder()
                .eventBout("how about that")
                .build();
        final Fight fightOfTheCentury = Fight.builder()
                .geoVenueLocation("Mars")
                .build();

        when(fightStoreService.getLatestFights())
                .thenReturn(Flux.just(fightOfTheNight, fightOfTheCentury));

        final FluxExchangeResult<Fight> result = client.get()
                .uri("/api/fights")
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus()
                .is2xxSuccessful()
                .expectHeader()
                .contentTypeCompatibleWith(MediaType.APPLICATION_JSON)
                .returnResult(Fight.class);

        StepVerifier.create(result.getResponseBody())
                .expectNext(fightOfTheNight)
                .expectNext(fightOfTheCentury)
                .verifyComplete();
    }

    @Test
    void getFightById() {
        final ObjectId fightId = new ObjectId();
        final Fight fightWithObjectId = Fight.builder()
                ._id(fightId)
                .build();

        final String fightIdString = fightId.toString();

        when(fightStoreService.getFightById(fightIdString))
                .thenReturn(Mono.just(fightWithObjectId));

        final FluxExchangeResult<Fight> result = client.get()
                .uri("/api/fights/{id}", newHashMap("id", fightIdString))
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus()
                .is2xxSuccessful()
                .expectHeader()
                .contentTypeCompatibleWith(MediaType.APPLICATION_JSON)
                .returnResult(Fight.class);

        StepVerifier.create(result.getResponseBody())
                .expectNext(fightWithObjectId)
                .verifyComplete();
    }

    @Test
    void serializesObjectId_asString() {
        final ObjectId fightId = new ObjectId(nowDate);
        final Fight fightWithObjectId = Fight.builder()
                ._id(fightId)
                .build();

        final String fightIdString = fightId.toString();

        when(fightStoreService.getFightById(fightIdString))
                .thenReturn(Mono.just(fightWithObjectId));

        client.get()
                .uri("/api/fights/{id}", newHashMap("id", fightIdString))
                .exchange()
                .expectBody()
                .jsonPath("_id")
                .isEqualTo(fightIdString);
    }

    @Test
    void serializesTimestamp_fromObjectId() throws IOException {
        final ObjectId fightId = new ObjectId(nowDate);
        final Fight fightWithObjectId = Fight.builder()
                ._id(fightId)
                .build();

        final String fightIdString = fightId.toString();

        when(fightStoreService.getFightById(fightIdString))
                .thenReturn(Mono.just(fightWithObjectId));

        final int expectedTimestamp = fightId.getTimestamp();

        final EntityExchangeResult<byte[]> resultBytes = client.get()
                .uri("/api/fights/{id}", newHashMap("id", fightIdString))
                .exchange()
                .expectBody()
                .jsonPath("_timestamp")
                .isEqualTo(expectedTimestamp)
                .returnResult();

        final Fight deserializedFight = new ObjectMapper().readValue(resultBytes.getResponseBody(), Fight.class);

        assertThat(deserializedFight).isNotNull();
        assertThat(deserializedFight.get_timestamp()).isEqualTo(expectedTimestamp);
    }

    @Test
    void serializesEventUrlPath_unencoded() {
        final ObjectId fightId = new ObjectId(nowDate);
        final String screwyUrlPath = "/!@#$%^&*()-_}{[]><<>\\'';'\"/hi";
        final Fight fightWithScrewyUrl = Fight.builder()
                ._id(fightId)
                .eventUrlPath(screwyUrlPath)
                .build();

        final String fightIdString = fightId.toString();

        when(fightStoreService.getFightById(fightIdString))
                .thenReturn(Mono.just(fightWithScrewyUrl));

        client.get()
                .uri("/api/fights/{id}", newHashMap("id", fightIdString))
                .exchange()
                .expectBody()
                .jsonPath("eventUrlPath")
                .isEqualTo("/!@#$%^&*()-_}{[]><<>\\'';'\"/hi");
    }
}
