package com.howlongtilthefight.fightstore.domain;

import org.bson.types.ObjectId;
import org.junit.jupiter.api.Test;

import java.time.Instant;
import java.util.Date;

import static org.assertj.core.api.Assertions.assertThat;

class FightTest {

    @Test
    void localInstantiation_doesNotGenerateObjectId() {
        final Fight fightFromCtor = new Fight();
        assertThat(fightFromCtor.get_id()).isNull();

        final Fight fightFromBuilder = Fight.builder().build();
        assertThat(fightFromBuilder.get_id()).isNull();
    }

    @Test
    void get_timestamp_matchesIdTimestamp() {
        final Instant instant = Instant.ofEpochSecond(12349876);
        final Fight fight = Fight.builder()
                ._id(new ObjectId(Date.from(instant)))
                .build();

        assertThat(fight.get_timestamp()).isEqualTo(12349876);
    }

    @Test
    void get_timestamp_whenIdIsNull_defaultsToZero() {
        final Fight fight = Fight.builder()
                ._id(null)
                .build();

        assertThat(fight.get_timestamp()).isEqualTo(0);
    }
}
