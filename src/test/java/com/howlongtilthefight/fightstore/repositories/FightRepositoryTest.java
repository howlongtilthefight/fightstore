package com.howlongtilthefight.fightstore.repositories;

import com.howlongtilthefight.fightstore.domain.Fight;
import org.bson.types.ObjectId;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.test.context.ActiveProfiles;
import reactor.test.StepVerifier;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.Month;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Date;

import static org.assertj.core.api.Assertions.assertThat;

@DataMongoTest
@ActiveProfiles(profiles = "repotests")
class FightRepositoryTest {

    @Autowired
    private FightRepository repository;

    @AfterEach
    void cleanUp() {
        repository.deleteAll().block();
    }

    @Test
    void insertAndFindAll() {
        final Fight fight = Fight.builder()
                .eventDate(Date.from(Instant.ofEpochMilli(999999L)))
                .eventName("cool event")
                .build();

        StepVerifier.create(repository.save(fight))
                .expectNext(fight)
                .verifyComplete();

        StepVerifier.create(repository.findAll())
                .expectNext(fight)
                .verifyComplete();
    }

    @Test
    void save_generatesObjectId() {
        final Date eventDate = Date.from(Instant.ofEpochSecond(12345678));
        final String eventName = "i want an ID";
        final Fight newFightObject = Fight.builder()
                .eventDate(eventDate)
                .eventName(eventName)
                .build();

        assertThat(newFightObject.get_id()).isNull();

        StepVerifier.create(repository.save(newFightObject))
                .assertNext(insertedFightObject -> {
                    assertThat(insertedFightObject.get_id()).matches(id -> ObjectId.isValid(id.toString()));

                    assertThat(insertedFightObject.getEventDate()).isEqualTo(eventDate);
                    assertThat(insertedFightObject.getEventName()).isEqualTo(eventName);
                })
                .verifyComplete();
    }

    @Test
    void save_updatesAndReturnsReference() {
        final Fight fightObject = Fight.builder().build();

        StepVerifier.create(repository.save(fightObject))
                .assertNext(insertedFightObject -> assertThat(fightObject).isSameAs(insertedFightObject))
                .verifyComplete();

        fightObject.setEventBout("bout time");
        StepVerifier.create(repository.save(fightObject))
                .assertNext(updatedFightObject -> assertThat(fightObject).isSameAs(updatedFightObject))
                .verifyComplete();

        StepVerifier.create(repository.findById(fightObject.get_id().toString()))
                .assertNext(foundFightObject -> assertThat(foundFightObject.getEventBout()).isEqualTo("bout time"))
                .verifyComplete();
    }

    @Nested
    class DateTests {
        private final ZonedDateTime nowUtc = ZonedDateTime.of(
                LocalDate.of(2000, Month.MAY, 5),
                LocalTime.NOON,
                ZoneId.of("UTC"));

        private final Date nowDate = Date.from(nowUtc.toInstant());
        private final Date pastDate = Date.from(nowUtc.minusHours(6).toInstant());
        private final Date futureDate = Date.from(nowUtc.plusHours(6).toInstant());

        private Fight insertedNowFight;
        private Fight insertedPastFight;
        private Fight insertedFutureFight;

        @BeforeEach
        void setUp() {
            final Fight nowFight = Fight.builder().eventDate(nowDate).eventName("Now event").build();
            final Fight pastFight = Fight.builder().eventDate(pastDate).eventName("Past event").build();
            final Fight futureFight = Fight.builder().eventDate(futureDate).eventName("Future event").build();

            insertedNowFight = repository.save(nowFight).block();
            insertedPastFight = repository.save(pastFight).block();
            insertedFutureFight = repository.save(futureFight).block();
        }

        @Test
        void findUpcomingEvents() {
            StepVerifier.create(repository.findAllByEventDateAfter(nowDate))
                    .expectNext(insertedFutureFight)
                    .verifyComplete();
        }

        @Test
        void findPastEvents() {
            StepVerifier.create(repository.findAllByEventDateBefore(nowDate))
                    .expectNext(insertedPastFight)
                    .verifyComplete();
        }

        @Test
        void findTodaysEvents() {
            final Date yesterdayDate = Date.from(nowUtc.minusDays(1).toInstant());
            final Date tomorrowDate = Date.from(nowUtc.plusDays(1).toInstant());
            final Fight yesterdayFight = repository.save(
                    Fight.builder().eventDate(yesterdayDate).eventName("Yesterday event").build()).block();
            final Fight tomorrowFight = repository.save(
                    Fight.builder().eventDate(tomorrowDate).eventName("Tomorrow event").build()).block();

            StepVerifier.create(repository.findAll())
                    .expectNext(insertedNowFight, insertedPastFight, insertedFutureFight)
                    .expectNext(yesterdayFight, tomorrowFight)
                    .verifyComplete();

            final Date nowDateBegin = Date.from(nowUtc.with(LocalTime.MIN).toInstant());
            final Date nowDateEnd = Date.from(nowUtc.with(LocalTime.MAX).toInstant());
            StepVerifier.create(repository.findAllByEventDateBetween(nowDateBegin, nowDateEnd))
                    .expectNext(insertedNowFight)
                    .expectNext(insertedPastFight)
                    .expectNext(insertedFutureFight)
                    .verifyComplete();
        }
    }
}
