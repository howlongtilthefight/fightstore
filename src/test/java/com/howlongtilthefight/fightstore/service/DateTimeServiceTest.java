package com.howlongtilthefight.fightstore.service;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.test.annotation.DirtiesContext;

import java.time.Clock;
import java.time.Instant;
import java.time.ZoneOffset;

import static org.assertj.core.api.Assertions.assertThat;

@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_CLASS)
class DateTimeServiceTest {

    private static Clock clock;
    private static DateTimeService dateTimeService;

    @BeforeAll
    static void setUp() {
        clock = Clock.fixed(Instant.parse("2016-04-20T06:09:10Z"), ZoneOffset.UTC);
        dateTimeService = new DateTimeService(clock);
    }

    @AfterAll
    static void cleanUp() {
        clock = Clock.systemUTC();
    }

    @Test
    void now_returnsCurrentDate() {
        final long expectedNowMillis = 1461132550000L;
        final long actualNowMillis = dateTimeService.now().toInstant().toEpochMilli();

        assertThat(actualNowMillis).isEqualTo(expectedNowMillis);
    }

    @Test
    void getCurrentYear_returnsCurrentYear() {
        final int expectedYear = 2016;

        assertThat(dateTimeService.getCurrentYear()).isEqualTo(expectedYear);
    }
}
