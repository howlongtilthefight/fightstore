package com.howlongtilthefight.fightstore.service;

import com.howlongtilthefight.fightstore.domain.Fight;
import com.howlongtilthefight.fightstore.repositories.FightRepository;
import com.howlongtilthefight.fightstore.workhorse.PaperScraperService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import reactor.core.publisher.Flux;
import reactor.test.StepVerifier;

import java.time.Instant;
import java.util.Date;

import static org.assertj.core.api.Assertions.assertThat;

@DataMongoTest
@ActiveProfiles(profiles = "repotests")
class FightStoreServiceIntTest {

    private FightStoreService fightStoreService;

    @Autowired
    private FightRepository repository;

    @MockBean
    private PaperScraperService paperScraper;

    @BeforeEach
    void setUp() {
        fightStoreService = new FightStoreService(repository, paperScraper);
    }

    @AfterEach
    void cleanUp() {
        repository.deleteAll().block();
    }

    @Test
    void storeFights_insertsAndUpdates_byUniquenessOf_eventUrlPath() {
        final Fight existingFight = repository.save(
                Fight.builder()
                        .eventUrlPath("/me?/well im a old timey fight/but some updates are in order")
                        .eventDate(Date.from(Instant.ofEpochSecond(22222222)))
                        .eventName("Terrible twos")
                        .geoVenue("The Tavern")
                        .build())
                .block();

        assertThat(existingFight).isNotNull();

        final Fight existingFightWithUpdates = Fight.builder()
                .eventUrlPath("/me?/well im a old timey fight/but some updates are in order")
                .eventDate(Date.from(Instant.ofEpochSecond(44444444)))
                .eventName("Ferocious fours")
                .geoVenue("A New Joint")
                .geoVenueLocation("Sorry we got the date wrong first try")
                .build();

        final Fight newFight = Fight.builder()
                .eventUrlPath("/im/a/brand/new/fight/WOOOOOOO")
                .eventDate(Date.from(Instant.ofEpochSecond(33333333)))
                .eventName("Three three repeating of course")
                .eventOrg("The police department")
                .geoVenue("Jail")
                .build();

        fightStoreService.storeFights(Flux.just(existingFightWithUpdates, newFight)).blockLast();

        StepVerifier.create(repository.findById(existingFight.get_id().toString()))
                .assertNext(updatedFight -> {
                    assertThat(updatedFight.get_id()).isEqualTo(existingFight.get_id());
                    assertThat(updatedFight.getEventDate())
                            .isEqualToIgnoringMillis(Date.from(Instant.ofEpochSecond(44444444)));
                    assertThat(updatedFight.getEventName()).isEqualTo("Ferocious fours");
                    assertThat(updatedFight.getGeoVenue()).isEqualTo("A New Joint");
                    assertThat(updatedFight.getGeoVenueLocation()).isEqualTo("Sorry we got the date wrong first try");
                })
                .verifyComplete();

        StepVerifier.create(repository.findById(newFight.get_id().toString()))
                .expectNext(newFight)
                .verifyComplete();

        StepVerifier.create(repository.findAll())
                .expectNextCount(2)
                .verifyComplete();
    }

    @Test
    void storeFights_whenEventUrlPathMissing_doesNotInsertOrUpdate() {
        final Fight existingFight = repository.save(
                Fight.builder()
                        .eventUrlPath("/lookatme/ihaveakey")
                        .eventDate(Date.from(Instant.ofEpochSecond(22222222)))
                        .eventName("Terrible twos")
                        .geoVenue("The Tavern")
                        .build())
                .block();

        assertThat(existingFight).isNotNull();

        final Fight existingFightWithUpdatesButNoUrl = Fight.builder()
                .eventDate(Date.from(Instant.ofEpochSecond(22222222)))
                .eventName("Terrible twos")
                .eventBout("I deleted the url path how 'bout that?")
                .geoVenue("A New Joint")
                .geoVenueLocation("Alan please add location")
                .build();

        final Fight newFightWithNoUrl = Fight.builder()
                .eventDate(Date.from(Instant.ofEpochSecond(33333333)))
                .eventName("Three three repeating of course")
                .build();

        fightStoreService.storeFights(Flux.just(existingFightWithUpdatesButNoUrl, newFightWithNoUrl)).blockLast();

        StepVerifier.create(repository.findById(existingFight.get_id().toString()))
                .assertNext(updatedFight -> {
                    assertThat(updatedFight.get_id()).isEqualTo(existingFight.get_id());
                    assertThat(updatedFight.getEventName()).isEqualTo("Terrible twos");
                    assertThat(updatedFight.getGeoVenue())
                            .isNotEqualTo("A New Joint")
                            .isEqualTo("The Tavern");
                    assertThat(updatedFight.getGeoVenueLocation()).isNull();
                    assertThat(updatedFight.getEventBout()).isNull();
                })
                .verifyComplete();

        assertThat(newFightWithNoUrl.get_id()).isNull();

        StepVerifier.create(repository.findAll())
                .expectNext(existingFight)
                .verifyComplete();
    }
}
