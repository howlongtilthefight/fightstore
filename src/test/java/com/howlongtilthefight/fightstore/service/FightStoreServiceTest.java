package com.howlongtilthefight.fightstore.service;

import com.howlongtilthefight.fightstore.domain.Fight;
import com.howlongtilthefight.fightstore.repositories.FightRepository;
import com.howlongtilthefight.fightstore.workhorse.PaperScraperService;
import org.bson.types.ObjectId;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.time.Duration;
import java.time.Instant;
import java.util.Date;

import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class FightStoreServiceTest {

    private final Instant now = Instant.parse("2016-04-20T06:09:10Z");
    private final Date nowDate = Date.from(now);

    private FightStoreService fightStoreService;

    @Mock
    private FightRepository repository;

    @Mock
    private PaperScraperService scraperService;

    @BeforeEach
    void setUp() {
        fightStoreService = new FightStoreService(repository, scraperService);
    }

    @Test
    void getFightById() {
        final ObjectId id = new ObjectId();
        final Fight expectedFight = Fight.builder()
                ._id(id)
                .eventName("Ivan I. D.")
                .build();

        when(repository.findById(id.toString()))
                .thenReturn(Mono.just(expectedFight));

        StepVerifier.create(fightStoreService.getFightById(id.toString()))
                .expectNext(expectedFight)
                .verifyComplete();
    }

    @Test
    void getLatestFights_whenRequestIsEmpty_fallsBackToDb() {
        final Fight nowFight = Fight.builder()
                ._id(new ObjectId())
                .eventDate(nowDate)
                .eventName("Now event")
                .build();
        final Fight tomorrowFight = Fight.builder()
                ._id(new ObjectId())
                .eventDate(Date.from(now.plus(Duration.ofDays(1))))
                .eventName("Tomorrow event")
                .build();
        final Fight nextWeekFight = Fight.builder()
                ._id(new ObjectId())
                .eventDate(Date.from(now.plus(Duration.ofDays(7))))
                .eventName("Next week event")
                .build();

        when(scraperService.requestFights())
                .thenReturn(Mono.empty());
        when(scraperService.getCurrentDate())
                .thenReturn(nowDate);
        when(repository.findAllByEventDateAfter(nowDate))
                .thenReturn(Flux.just(nowFight, tomorrowFight, nextWeekFight));

        StepVerifier.create(fightStoreService.getLatestFights())
                .expectNext(nowFight, tomorrowFight, nextWeekFight)
                .then(() -> verify(repository, never()).saveAll(ArgumentMatchers.<Flux<? extends Fight>>any()))
                .verifyComplete();
    }

    @Test
    void getLatestFights_whenRequestHasData_persistsNewData() {
        when(scraperService.getCurrentDate())
                .thenReturn(nowDate);

        final Fight nowFight = Fight.builder()
                ._id(new ObjectId())
                .eventDate(nowDate)
                .eventName("Now event")
                .build();
        final Fight tomorrowFightUpdated = Fight.builder()
                ._id(new ObjectId())
                .eventDate(Date.from(now.plus(Duration.ofDays(1))))
                .eventName("Tomorrow event")
                .geoCountry("Party World")
                .build();
        final Fight nextWeekFight = Fight.builder()
                ._id(new ObjectId())
                .eventDate(Date.from(now.plus(Duration.ofDays(7))))
                .eventName("Next week event")
                .build();

        final Flux<Fight> updatedFlux = Flux.just(nowFight, tomorrowFightUpdated, nextWeekFight);
        when(scraperService.requestFights())
                .thenReturn(updatedFlux);
        when(repository.saveAll(ArgumentMatchers.<Flux<Fight>>any()))
                .thenReturn(updatedFlux);
        when(repository.findAllByEventDateAfter(nowDate))
                .thenReturn(updatedFlux);

        StepVerifier.create(fightStoreService.getLatestFights())
                .expectNext(nowFight, tomorrowFightUpdated, nextWeekFight)
                .then(() -> verify(repository).saveAll(ArgumentMatchers.<Flux<Fight>>any()))
                .then(() -> verifyNoMoreInteractions(repository))
                .verifyComplete();
    }

    @Test
    void storeFights_new() {
        final Fight.FightBuilder fightOneBuilder = Fight.builder()
                .eventName("I am #1");
        final Fight.FightBuilder fightTwoBuilder = Fight.builder()
                .eventName("Number 2 4 u");

        final Flux<Fight> newFights = Flux.just(fightOneBuilder.build(), fightTwoBuilder.build());

        final Fight persistedFightOne = fightOneBuilder._id(new ObjectId()).build();
        final Fight persistedFightTwo = fightTwoBuilder._id(new ObjectId()).build();

        when(repository.saveAll(ArgumentMatchers.<Flux<Fight>>any()))
                .thenReturn(Flux.just(persistedFightOne, persistedFightTwo));

        StepVerifier.create(fightStoreService.storeFights(newFights))
                .expectNext(persistedFightOne)
                .expectNext(persistedFightTwo)
                .verifyComplete();
    }
}
