package com.howlongtilthefight.fightstore.workhorse;

import com.howlongtilthefight.fightstore.client.TapologyClient;
import com.howlongtilthefight.fightstore.domain.Fight;
import com.howlongtilthefight.fightstore.service.DateTimeService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import reactor.core.publisher.Flux;
import reactor.test.StepVerifier;

import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Date;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class PaperScraperServiceTest {

    private static final Integer TEST_YEAR = 2018;
    private static final Instant now = ZonedDateTime
            .of(LocalDateTime.of(TEST_YEAR, Month.FEBRUARY, 2, 21, 0), ZoneId.of("UTC"))
            .toInstant();
    private static final Date nowDate = Date.from(now);

    @Mock
    private DateTimeService dateTimeService;

    @Mock
    private TapologyClient tapologyClient;

    private PaperScraperService subject;

    private Fight fight1;
    private Fight fight2;

    @BeforeEach
    void setUp() {
        fight1 = Fight.builder()
                .eventDate(new Date(12345678L))
                .eventUrlPath("/jumpworld/comevisitus!/smackeroohoo")
                .eventName("The Smackeroo")
                .build();
        fight2 = Fight.builder()
                .eventUrlPath("/fightcenter/events/oofowie123")
                .eventDate(new Date(87654321L))
                .eventName("Oofapalooza")
                .geoRegion("Greater 'Ouchie Town' Area")
                .build();
    }

    @Test
    void requestFights_rateLimiter_beforeOneHour_returnsEmpty() {
        when(dateTimeService.now())
                .thenReturn(nowDate);

        subject = new PaperScraperService(dateTimeService, tapologyClient);

        when(dateTimeService.now())
                .thenReturn(Date.from(now.plus(Duration.ofMinutes(59))));

        StepVerifier.create(subject.requestFights())
                .then(() -> verifyNoInteractions(tapologyClient))
                .verifyComplete();
    }

    @Test
    void requestFights_rateLimiter_afterOneHour_fetchesNewData() {
        when(dateTimeService.now())
                .thenReturn(nowDate);

        subject = new PaperScraperService(dateTimeService, tapologyClient);

        when(dateTimeService.now())
                .thenReturn(Date.from(now.plus(Duration.ofMinutes(61))));
        when(tapologyClient.getEvents(TEST_YEAR))
                .thenReturn(Flux.just(fight1, fight2));

        StepVerifier.create(subject.requestFights())
                .expectNext(fight1)
                .expectNext(fight2)
                .then(() -> {
                    verify(tapologyClient, times(1)).getEvents(TEST_YEAR);
                    verifyNoMoreInteractions(tapologyClient);
                })
                .verifyComplete();
    }

    @Test
    void getCurrentDate_returnsCurrentYear() {
        when(dateTimeService.now())
                .thenReturn(nowDate);

        subject = new PaperScraperService(dateTimeService, tapologyClient);

        assertThat(subject.getCurrentDate()).isEqualTo(nowDate);
    }
}
